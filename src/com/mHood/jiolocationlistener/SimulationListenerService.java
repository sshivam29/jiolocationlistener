package com.mHood.jiolocationlistener;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.channels.AsynchronousCloseException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import javax.net.ssl.SSLContext;



import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;


import com.jio.mhood.services.JioServiceManager;
import com.jio.mhood.services.JioServiceManager.JioServiceInitCallback;
import com.jio.mhood.services.api.accounts.login.AuthenticationManager;
import com.jio.mhood.services.api.connection.ErrorResponse;


import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings.Secure;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class SimulationListenerService extends Service {

	double  lat, lon;
	int timeinterval= 0;
	static int counter = 0;	
	private Timer myTimer;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		Log.e("onStartCommand","Err------------RecieverRecieved:onStartCommand");
		JioServiceManager.init(this, new JioServiceCB());
		Bundle iData = intent.getExtras();
		if(iData != null)
		{
			mSsoToken = iData.getString("sso");
			lat = iData.getDouble("lat");
			lon = iData.getDouble("lon");
			timeinterval = iData.getInt("interval");
		}
		final String CONTEXT = "onStartCommand";
        String ssoToken = mSsoToken;
        Log.i(TAG, CONTEXT + "> intent: " + intent + "; flags: " + flags + "; startId: " + startId + "; ssoToken: " + ssoToken);

	        mHandler = new Handler();
	        myTimer = new Timer();
	     
		        myTimer.schedule(new TimerTask() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						ScheduledSendingMethod();
	
					}
				},0,timeinterval*60);
		     super.onStartCommand(intent, flags, startId);
		     return Service.START_NOT_STICKY;
	}
	private void ScheduledSendingMethod()
	{
		String result = addLocationToUrl("test");
		try {
			JSONObject aJsonResponse = new JSONObject(result);
			String RespType = aJsonResponse.getString("responseType");
			String RespVal = aJsonResponse.getString("ok");
			//Toast.makeText(SimulationListenerService.this, "Response Type:"+RespType+"Resp Val:"+RespVal, Toast.LENGTH_LONG).show();
			Log.d("ScheduledSendingMethod","Response Type:"+RespType+"Resp Val:"+RespVal);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	class JioServiceCB implements JioServiceInitCallback{

		@Override
		public void onJioServicesFailed() {
			// TODO Auto-generated method stub
			Toast.makeText(SimulationListenerService.this,"onJioServicesFailed()", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onJioServicesReady() {
			// TODO Auto-generated method stub
			 mMdkClientApi = (AuthenticationManager) JioServiceManager
		                .getJioService(JioServiceManager.AUTHENTICATION_MANAGER);
			 GetSsoTokenTask aTask = new GetSsoTokenTask();
		        aTask.execute();

			 
		}
    	
    }

	public static String getDeviceUid(Context c) {
        // Get the mac address and use it as deviceuid
        WifiManager wifiMan = (WifiManager) c.getSystemService(
                Context.WIFI_SERVICE);
        WifiInfo wifiInf = wifiMan.getConnectionInfo();
        String id = wifiInf.getMacAddress();

        if(id == null || id.equals("")) {
            Log.d("Test", "Failed to get the mac addr! Try with imei..");

            // Mac not found .. try imei
            TelephonyManager telephonyManager =
                    (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
            id = telephonyManager.getDeviceId();
        }

        if(id == null || id.equals("")) {
            Log.e("Test", "Failed to get mac or imei! Let use the Android_ID");

            // REMOVED this Assert, not sure why this is in non-test code?? -Ryan
            // Asserting failure, mac or imei should always be found!
            //Assert.fail("MAC address or IMEI cannot be received!");

            id=  Secure.getString(c.getContentResolver(),Secure.ANDROID_ID);
        }

        UUID uuid = null;

        try {
            uuid = UUID.nameUUIDFromBytes(id.getBytes("utf8"));
        } catch (UnsupportedEncodingException e) {
            Log.e("Test", "Failed convert to uuid");
        }

        if(uuid == null) {
            return id;
        } else {
            Log.d("Test", "uuid = " + uuid.toString());

            return uuid.toString();
        }
    }
	
	protected String addLocationToUrl(String url){
	    String response = null;
	    JSONObject body = new JSONObject();
	    try {
			body.put("coreContainerUuid", getDeviceUid(SimulationListenerService.this)/*"9c1b11e9-ae6f-3396-9b8d-de570ceeb6ea"*/);
			body.put("ssoToken", mSsoToken);
			//body.put("ssoToken", "AQIC5wM2LY4SfcxE6qowtO2J-iGebwDgGgjRZtw49IQbTfA.*AAJTSQACMDIAAlMxAAIwMQ..*");
			body.put("latitude", lat);
			body.put("longitude", lon);
			body.put("radius", 2.00);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    System.out.println("addLocationToUrl() Before Http call ");
	    HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("https://116.50.77.28:8443/servers-panelWebService/rest/location/update");

        try {
			//httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			httpPost.addHeader("x-server-api-key", "qwertyuiopasdfghjklzxcvbnm0123456789");
			httpPost.addHeader("x-device-os", "ANDROID");
			httpPost.addHeader("x-device-type", "PHONE");
			httpPost.setHeader("Content-type", "application/json");
			StringEntity se = new StringEntity(body.toString());
			 
			HttpClient client1 = new DefaultHttpClient();
			httpClient = getSslClient(client1);
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			
		        HttpEntity httpEntity = httpResponse.getEntity();
		        response = EntityUtils.toString(httpEntity);
		        System.out.println("Response is "+response);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
        catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return response;
	}
	public HttpClient getSslClient(HttpClient client)
			throws KeyManagementException, UnrecoverableKeyException,
			NoSuchAlgorithmException, KeyStoreException {

		SSLContext sslContext = SSLContext.getInstance("TLS");
		SSLSocketFactory ssf = new IdentitySSLSocketFactory(sslContext);
		ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		ClientConnectionManager ccm = client.getConnectionManager();
		SchemeRegistry sr = ccm.getSchemeRegistry();
		sr.register(new Scheme("https", ssf, 8443));
		return new DefaultHttpClient(ccm, client.getParams());
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.e("onDestroy","Err----SIMULATION--------onDestroy() called");
		
		if(myTimer != null)
		{
			myTimer.cancel();
		}
		final String CONTEXT = "onDestroy";
        Log.d(TAG, CONTEXT + "> ");
        if(mLoginReceiver != null)
        {
	        try {
	            unregisterReceiver(mLoginReceiver);
	        } catch (Exception e) {
	            Log.e(TAG, CONTEXT, e);
	        }
        }

		super.onDestroy();
	}
	
	 private static final String TAG = "SsoService";

	    private AuthenticationManager mMdkClientApi;
	    private static final String SERVICE_ID = "00001";
	    private String mSsoToken;
	    private BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	            if (intent.getAction().equals(AuthenticationManager.INTENT_ACTION_JIO_LOGIN_FINISHED)) {
	                try {
	                    unregisterReceiver(this);
	                } catch (Exception e) {

	                }

	                // Retry getting SSO Token ...
	                GetSsoTokenTask aTask = new GetSsoTokenTask();
	                aTask.execute();
	            }
	        }
	    };

	    private IntentFilter mLoginFilter = new IntentFilter(AuthenticationManager.INTENT_ACTION_JIO_LOGIN_FINISHED);
	    private Handler mHandler;

	    private final AuthenticationManager.AuthenticationListener mAuthenticationListener =
	            new AuthenticationManager.AuthenticationListener() {

	        /**
	         * Positive callback for ssoToken request
	         */
	        @Override
	        public void onTokenReceived(final String ssoToken) {
	            mSsoToken = ssoToken;

	            mHandler.post(new Runnable() {
	                @Override
	                public void run() {
	                  //  Toast.makeText(getApplicationContext(), "SSO Token: " + ssoToken, Toast.LENGTH_LONG).show();
	                    JSONObject input = new JSONObject();
	                    Log.d("onTokenReceived","SSO Token:"+ssoToken);
	                    try {
							input.put("latitude", lat);
							input.put("longitude", lon);
		                    input.put("ssoToken", mSsoToken);
		                    input.put("serverdownload", true);
		                    
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	                    
	                   // new asyncTask().execute("null");
	                    JioLocationAsyncTask iJiopoStAsyncTask = new JioLocationAsyncTask(getApplicationContext());
	                    iJiopoStAsyncTask.execute(input);
	                }
	            });

	        }

	        /**
	         * User interaction requested to complete ssoToken request
	         *
	         * @param requestIntent
	         * @param requestCode
	         */
	        @Override
	        public void onActivityRequest(Intent requestIntent, int requestCode) {
	            final String CONTEXT = "onActivityRequest";
	            Log.i(TAG, CONTEXT + "> requestIntent: " + requestIntent + "; requestCode: " + requestCode);

	            try {
	                registerReceiver(mLoginReceiver, mLoginFilter);
	            } catch (Exception e) {
	                Log.e(TAG, CONTEXT, e);
	            }

	            mHandler.post(new Runnable() {
	                @Override
	                public void run() {
	                    Toast.makeText(getApplicationContext(), "User activity requested!", Toast.LENGTH_LONG).show();
	                }
	            });
	        }

	    };

	   

	    /* (non-Javadoc)
	     * @see android.app.Service#onCreate()
	     */
	    @Override
	    public void onCreate() {
	        final String CONTEXT = "onCreate";
	        Log.i(TAG, CONTEXT + "> ");
	        super.onCreate();

	        mMdkClientApi = (AuthenticationManager) JioServiceManager
	                .getJioService(JioServiceManager.AUTHENTICATION_MANAGER);
	    }

	    private class GetSsoTokenTask extends AsyncTask<Void, Void, Void> {
	        @Override
	        protected Void doInBackground(Void... object) {
	            final String CONTEXT = "GetSsoTokenTask.doInBackground";
	            Log.d(TAG, CONTEXT + "> ");

	            getSsoToken();

	            return null;
	        }

	      }

	    /**
	     * Negative/error callback for ssoToken request
	     */
	    private final Handler mErrorHandler = new Handler() {
	        @Override
	        public void handleMessage(Message msg) {
	            final ErrorResponse response = msg.getData().getParcelable(
	                    AuthenticationManager.JIO_ERROR);
	            String str = response.getMessage();
	            String aMessage = null;

	            if (str != null) {
	                aMessage = response.getMessage();
	            } else {
	                aMessage = "Not logged in.";
	            }

	            final String message = aMessage;

	            mHandler.post(new Runnable() {
	                @Override
	                public void run() {
	                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
	                }
	            });

	        }
	    };

	    /**
	     * Gets the SSO Token.
	     */
	    private void getSsoToken() {
	        mHandler.post(new Runnable() {
	            @Override
	            public void run() {
	                Toast.makeText(getApplicationContext(), "Invoking getSsoToken()....", Toast.LENGTH_LONG).show();
	            }
	        });
	        if(mMdkClientApi == null)
	        {
	        	 mMdkClientApi = (AuthenticationManager) JioServiceManager
			                .getJioService(JioServiceManager.AUTHENTICATION_MANAGER);
	        }
	        mMdkClientApi.getSsoTokenWithNotification(SERVICE_ID, null, mAuthenticationListener, mErrorHandler);
	    }
		
}
