package com.mHood.jiolocationlistener;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class SimulateLocationActivity extends Activity implements OnClickListener{
	Spinner spinner;
	ArrayList<String> list;
	ArrayList<Double> mLatituelist;
	ArrayList<Double> mLongitudelist;
	ProgressDialog iDialog;
	EditText coordinatesText;
	Button sendperiodic,sendonce;
	String mSsoToken = null;
	Double lat,lon;
	Intent SimulationServiceIntent;
	int timeInterval;
	ServerSettingsTask iTask = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_simulate_location);
		if(savedInstanceState == null)
		{
			Bundle iData = getIntent().getExtras();
			if(iData != null)
			{
				mSsoToken = iData.getString("sso");
				//Toast.makeText(this, "SSO:"+mSsoToken, Toast.LENGTH_SHORT).show();
			}
		}
		spinner = (Spinner) findViewById(R.id.spinner);
		list = new ArrayList<String>();
		mLatituelist = new ArrayList<Double>();
		mLongitudelist = new ArrayList<Double>();
		coordinatesText = (EditText) findViewById(R.id.coordinatesText);
		sendperiodic = (Button) findViewById(R.id.SendPeriodic);
		sendonce = (Button) findViewById(R.id.sendonetime);
		sendperiodic.setOnClickListener(this);
		sendonce.setOnClickListener(this);
		//mMdkClientApi.getSsoTokenWithNotification(SERVICE_ID, null, mAuthenticationListener, mErrorHandler);
		//addItemsOnSpinner();
		//fetch the data from the settings server and add them into 
		//the drop down list
		iTask = new ServerSettingsTask();
		iTask.execute("null");
	}
	
	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.SendPeriodic)
		{
			//we need to start the service which is running back ground
			SimulationServiceIntent = new Intent(this,SimulationListenerService.class);
			SimulationServiceIntent.setPackage("com.mHood.jiolocationlistener");
			SimulationServiceIntent.putExtra("sso", mSsoToken);
			SimulationServiceIntent.putExtra("lat", lat);
			SimulationServiceIntent.putExtra("lon", lon);
			SimulationServiceIntent.putExtra("interval", timeInterval*1000);
			startService(SimulationServiceIntent);
			sendperiodic.setEnabled(false);
		}
		else if(v.getId() == R.id.sendonetime)
		{
			//we need to send the data in once time

           // Toast.makeText(getApplicationContext(), "SSO Token: " + mSsoToken, Toast.LENGTH_LONG).show();
            JSONObject input = new JSONObject();
            Log.d("onClick sendonetime","SSO Token:"+mSsoToken);
            try {
				input.put("latitude", lat);
				input.put("longitude", lon);
                input.put("ssoToken", mSsoToken);
                input.put("serverdownload", false);
                
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
           // new asyncTask().execute("null");
            JioLocationAsyncTask iJiopoStAsyncTask = new JioLocationAsyncTask(getApplicationContext());
            iJiopoStAsyncTask.execute(input);
        
			
		}
		
	}
	 public void addItemsOnSpinner() {
		 
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner.setAdapter(dataAdapter);
		  }
	  public void addListenerOnSpinnerItemSelection() {
			spinner = (Spinner) findViewById(R.id.spinner);
			spinner.setOnItemSelectedListener(new CustomOnItemSelectedListener());
		  }
	  
	  public class CustomOnItemSelectedListener implements OnItemSelectedListener {
		  @Override
		  public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
			Toast.makeText(parent.getContext(), 
				"OnItemSelectedListener : " + parent.getItemAtPosition(pos).toString(),
				Toast.LENGTH_SHORT).show();
			if(coordinatesText != null)
			{
				String iString = "Latitude:"+mLatituelist.get(pos)+"\nLongitue:"+mLongitudelist.get(pos);
				lat = mLatituelist.get(pos);
				lon = mLongitudelist.get(pos);
				coordinatesText.setText(iString);
			}
		  }
		 
		  @Override
		  public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		  }

		
		 
		}
	  
	  class ServerSettingsTask extends AsyncTask<String, Integer, String>
	  {
		 @Override
		protected void onPreExecute() {
			iDialog = new ProgressDialog(SimulateLocationActivity.this);
			iDialog.setTitle("Loading...");
			iDialog.setMessage("Please wait.");
			iDialog.setCancelable(false);
			iDialog.setIndeterminate(true);
			iDialog.show();
			super.onPreExecute();
		} 
		@Override
		protected String doInBackground(String... params) {
			String result = null;
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet("https://script.google.com/macros/s/AKfycby2cU7od1SQt2ZrGszY_D0Zo-HloJrBpUaJQcmfnu4J-RrreUY/exec?tab=1"/*"https://api.datamarket.azure.com/Bing/Search/Web?Query='Anil'&$top=10&$skip=20&$format=JSON"*/);
			try {
				HttpResponse httpResponse = httpClient.execute(httpGet);
				HttpEntity entity = httpResponse.getEntity();
				result = EntityUtils.toString(entity);
		        System.out.println("Response is "+result);
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}
		  
		 @Override
		protected void onPostExecute(String result) {
			 if(result != null)
			 {
				 try {
					JSONArray iArray = new JSONArray(result);
					for(int i = 0; i < iArray.length()-1; i++)
					{
						JSONObject obj = iArray.getJSONObject(i);
						System.out.println("item"+i+":"+obj.getString("name"));
						list.add(obj.getString("name"));
						mLatituelist.add(obj.getDouble("latitude"));
						mLongitudelist.add(obj.getDouble("longitude"));						
						
						
					}
					timeInterval = iArray.getJSONObject(4).getInt("frequency");
					addItemsOnSpinner();
					addListenerOnSpinnerItemSelection();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 }
			 iDialog.dismiss();
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		}
	  }

	  @Override
	protected void onDestroy() {
		  Log.d("SimulateLocationActivity","OnDestroy Called");
		if(SimulationServiceIntent != null)
		{
			stopService(SimulationServiceIntent);
			Log.d("SimulateLocationActivity","SimulationService Stopped");
		}
		if(iTask != null)
		{
			iTask.cancel(true);
			Log.d("SimulateLocationActivity","AsyncTask Stopped forcefully");
		}
		super.onDestroy();
	}
}
