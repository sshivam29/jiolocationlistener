package com.mHood.jiolocationlistener;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.channels.AsynchronousCloseException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import javax.net.ssl.SSLContext;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;


import com.jio.mhood.services.JioServiceManager;
import com.jio.mhood.services.JioServiceManager.JioServiceInitCallback;
import com.jio.mhood.services.api.accounts.login.AuthenticationManager;
import com.jio.mhood.services.api.connection.ErrorResponse;


import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class JioLocationListenerService extends Service implements LocationListener{

	LocationManager locationManager = null;
	LocationListener loc_listener = null;
	double  lat, lon;
	static int counter = 0;
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		JioServiceManager.init(this, new JioServiceCB());
		final String CONTEXT = "onStartCommand";
        String ssoToken = mSsoToken;
        Log.i(TAG, CONTEXT + "> intent: " + intent + "; flags: " + flags + "; startId: " + startId + "; ssoToken: " + ssoToken);

	        mHandler = new Handler();
	        
		 locationManager = (LocationManager)getSystemService (LOCATION_SERVICE);
		 	 
		       
		    Criteria criteria = new Criteria ();
		    String bestProvider = locationManager.getBestProvider (criteria, false);
		    Location location = locationManager.getLastKnownLocation (bestProvider);
		    
		    locationManager.requestLocationUpdates( bestProvider,1000*60*5/*4000*/ ,0, this);
		    location = locationManager.getLastKnownLocation (bestProvider);   
		    if(location == null)
		    {
		    	location = getLastKnownLocation();
		    }
		    try
		    {
		    	
		        lat = location.getLatitude ();
		        lon = location.getLongitude ();
		        Log.d("JioLocationListenerService"," Lat:"+lat+"Lon:"+lon);
		       // new asyncTask().execute("null");
		        GetSsoTokenTask tTask = new GetSsoTokenTask();
		        tTask.execute();
		       
		    }
		    catch (NullPointerException e)
		    {
		        lat = -1.0;
		        lon = -1.0;
		    }
		    
		     super.onStartCommand(intent, flags, startId);
		     return Service.START_NOT_STICKY;
	}
	
	
	private Location getLastKnownLocation() {
	    List<String> providers = locationManager.getProviders(true);
	    Location bestLocation = null;
	    for (String provider : providers) {
	        Location l = locationManager.getLastKnownLocation(provider);
	        Log.d("last known location"," provider:"+provider+", location: "+l);

	        if (l == null) {
	            continue;
	        }
	        if (bestLocation == null
	                || l.getAccuracy() < bestLocation.getAccuracy()) {
	            Log.d("getLastKnownLocation","found best last known location:"+l);
	            bestLocation = l;
	        }
	    }
	    if (bestLocation == null) {
	        return null;
	    }
	    return bestLocation;
	}
	
	class JioServiceCB implements JioServiceInitCallback{

		@Override
		public void onJioServicesFailed() {
			// TODO Auto-generated method stub
			Toast.makeText(JioLocationListenerService.this,"onJioServicesFailed()", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onJioServicesReady() {
			// TODO Auto-generated method stub
			 mMdkClientApi = (AuthenticationManager) JioServiceManager
		                .getJioService(JioServiceManager.AUTHENTICATION_MANAGER);
			 GetSsoTokenTask aTask = new GetSsoTokenTask();
		        aTask.execute();

			 
		}
    	
    }
	
	public String getDeviceID()
	{
		TelephonyManager tManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
		String devId = tManager.getDeviceId();
		Log.e("getDeviceID","DeviceID------------"+devId);
		return devId;
	}
	
	
	

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.e("onDestroy","Err------------onDestroy() called");
		if(locationManager != null)
		{
			locationManager.removeUpdates(this);
			locationManager = null; 
		}
		final String CONTEXT = "onDestroy";
        Log.d(TAG, CONTEXT + "> ");
        if(mLoginReceiver != null)
        {
	        try {
	            unregisterReceiver(mLoginReceiver);
	        } catch (Exception e) {
	            Log.e(TAG, CONTEXT, e);
	        }
        }

		super.onDestroy();
	}
	
	 private static final String TAG = "SsoService";

	    private AuthenticationManager mMdkClientApi;
	    private static final String SERVICE_ID = "00001";
	    private String mSsoToken;
	    private BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	            if (intent.getAction().equals(AuthenticationManager.INTENT_ACTION_JIO_LOGIN_FINISHED)) {
	                try {
	                    unregisterReceiver(this);
	                } catch (Exception e) {

	                }

	                // Retry getting SSO Token ...
	                GetSsoTokenTask aTask = new GetSsoTokenTask();
	                aTask.execute();
	            }
	        }
	    };

	    private IntentFilter mLoginFilter = new IntentFilter(AuthenticationManager.INTENT_ACTION_JIO_LOGIN_FINISHED);
	    private Handler mHandler;

	    private final AuthenticationManager.AuthenticationListener mAuthenticationListener =
	            new AuthenticationManager.AuthenticationListener() {

	        /**
	         * Positive callback for ssoToken request
	         */
	        @Override
	        public void onTokenReceived(final String ssoToken) {
	            mSsoToken = ssoToken;

	            mHandler.post(new Runnable() {
	                @Override
	                public void run() {
	                    Toast.makeText(getApplicationContext(), "SSO Token: " + ssoToken, Toast.LENGTH_LONG).show();
	                    JSONObject input = new JSONObject();
	                    Log.d("onTokenReceived","SSO Token:"+ssoToken);
	                    try {
							input.put("latitude", lat);
							input.put("longitude", lon);
		                    input.put("ssoToken", mSsoToken);
		                    input.put("serverdownload", false);
		                    
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	                    
	                   // new asyncTask().execute("null");
	                    JioLocationAsyncTask iJiopoStAsyncTask = new JioLocationAsyncTask(getApplicationContext());
	                    iJiopoStAsyncTask.execute(input);
	                }
	            });

	        }

	        /**
	         * User interaction requested to complete ssoToken request
	         *
	         * @param requestIntent
	         * @param requestCode
	         */
	        @Override
	        public void onActivityRequest(Intent requestIntent, int requestCode) {
	            final String CONTEXT = "onActivityRequest";
	            Log.i(TAG, CONTEXT + "> requestIntent: " + requestIntent + "; requestCode: " + requestCode);

	            try {
	                registerReceiver(mLoginReceiver, mLoginFilter);
	            } catch (Exception e) {
	                Log.e(TAG, CONTEXT, e);
	            }

	            mHandler.post(new Runnable() {
	                @Override
	                public void run() {
	                    Toast.makeText(getApplicationContext(), "User activity requested!", Toast.LENGTH_LONG).show();
	                }
	            });
	        }

	    };

	   

	    /* (non-Javadoc)
	     * @see android.app.Service#onCreate()
	     */
	    @Override
	    public void onCreate() {
	        final String CONTEXT = "onCreate";
	        Log.i(TAG, CONTEXT + "> ");
	        super.onCreate();

	        mMdkClientApi = (AuthenticationManager) JioServiceManager
	                .getJioService(JioServiceManager.AUTHENTICATION_MANAGER);
	    }

	    private class GetSsoTokenTask extends AsyncTask<Void, Void, Void> {
	        @Override
	        protected Void doInBackground(Void... object) {
	            final String CONTEXT = "GetSsoTokenTask.doInBackground";
	            Log.d(TAG, CONTEXT + "> ");

	            getSsoToken();

	            return null;
	        }

	      }

	    /**
	     * Negative/error callback for ssoToken request
	     */
	    private final Handler mErrorHandler = new Handler() {
	        @Override
	        public void handleMessage(Message msg) {
	            final ErrorResponse response = msg.getData().getParcelable(
	                    AuthenticationManager.JIO_ERROR);
	            String str = response.getMessage();
	            String aMessage = null;

	            if (str != null) {
	                aMessage = response.getMessage();
	            } else {
	                aMessage = "Not logged in.";
	            }

	            final String message = aMessage;

	            mHandler.post(new Runnable() {
	                @Override
	                public void run() {
	                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
	                }
	            });

	        }
	    };

	    /**
	     * Gets the SSO Token.
	     */
	    private void getSsoToken() {
	        mHandler.post(new Runnable() {
	            @Override
	            public void run() {
	                Toast.makeText(getApplicationContext(), "Invoking getSsoToken()....", Toast.LENGTH_LONG).show();
	            }
	        });
	        if(mMdkClientApi == null)
	        {
	        	 mMdkClientApi = (AuthenticationManager) JioServiceManager
			                .getJioService(JioServiceManager.AUTHENTICATION_MANAGER);
	        }
	        mMdkClientApi.getSsoTokenWithNotification(SERVICE_ID, null, mAuthenticationListener, mErrorHandler);
	    }
		@Override
		public void onLocationChanged(Location location) {
			Log.d("onLocationChanged","Lat:"+location.getLatitude()+"Lon:"+location.getLongitude());
			lat = location.getLatitude();
			lon = location.getLongitude();
			// Toast.makeText(this,"Lat:"+location.getLatitude()+"Lon:"+location.getLongitude(), Toast.LENGTH_LONG).show();
			//"User_Opened_App"
			HashMap<String, String> segmentation = new HashMap<String, String>();
//			segmentation.put("sso", mSsoToken);
//			segmentation.put("simulation", "false");
			segmentation.put("lat-lon", lat+"-"+lon);
			//Countly.sharedInstance().recordEvent("TRACK_LOCATION", segmentation, 1);
			if(mSsoToken != null)
			{
			  JSONObject input = new JSONObject();
              Log.d("onTokenReceived","SSO Token:"+mSsoToken);
              try {
					input.put("latitude", lat);
					input.put("longitude", lon);
                  input.put("ssoToken", mSsoToken);
                  input.put("serverdownload", false);
                  JioLocationAsyncTask iJiopoStAsyncTask = new JioLocationAsyncTask(getApplicationContext());
                  iJiopoStAsyncTask.execute(input);
                  
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
			else
			{
				getSsoToken();
			}
             // new asyncTask().execute("null");
              
			
		}
		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}
}
