package com.mHood.jiolocationlistener;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.UUID;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class JioLocationAsyncTask extends AsyncTask<JSONObject, Integer, String>{
	double  lat, lon;
	String mSsoToken;
	Context mContext;
	boolean isServerConfig = false;
	JioLocationAsyncTask(Context context)
	{
		mContext = context;
	}
	@Override
	protected String doInBackground(JSONObject... params) {
		String result = null;
		//JSONObject aJsonObj;
		try {
			//aJsonObj = new JSONObject(params[0]);
			lat = params[0].getDouble("latitude");
			lon = params[0].getDouble("longitude");
			mSsoToken = params[0].getString("ssoToken");
			isServerConfig = params[0].getBoolean("serverdownload");
			
			//lat = aJsonObj.getDouble("latitude");
			//lon = aJsonObj.getDouble("longitude");
			//mSsoToken = aJsonObj.getString("ssoToken");
			if(isServerConfig == true)
			{
				result = testBingJson();
			}
			else
			{
				result = addLocationToUrl("null");////
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	private void getResposeValue(String result)
	{
		
		
		try {
			JSONObject aJsonResponse = new JSONObject(result);
			String RespType = aJsonResponse.getString("responseType");
			String RespVal = aJsonResponse.getString("ok");
			Toast.makeText(mContext, "Response Type:"+RespType+"Resp Val:"+RespVal, Toast.LENGTH_LONG).show();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void getLocationList(String result)
	{
		try {
			JSONArray aJsonResponse = new JSONArray(result);
			for(int i = 0 ; i < aJsonResponse.length(); i++)
			{
				JSONObject aJResponse = aJsonResponse.getJSONObject(i);
				//JSONArray aJRespLat = aJsonResponse.getJSONArray("latitude");
			}
			//String RespType = aJsonResponse.getString("responseType");
			String RespVal = aJsonResponse.getJSONObject(4).getString("frequency");
			Toast.makeText(mContext, "Response + frequency"+RespVal, Toast.LENGTH_LONG).show();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	protected void onPostExecute(String result) {
		if(result != null)
		{
			if(isServerConfig == true)
			{
				getLocationList(result);
			}
			else
			{
				getResposeValue(result);
			}
		
		}
		else
		{
			Toast.makeText(mContext, "Response Type:Server error", Toast.LENGTH_LONG).show();
		}
		super.onPostExecute(result);

	}
	private String testBingJson()
	{
		String result = null;
		//Key = oU2yjoGDz7WHcLbweVBGEG4mlSuAtwVp0XD4c1p84x8 Edit 
		 HttpClient httpClient = new DefaultHttpClient();
		// http://api.bing.net/xml.aspx?Appid=<AppID>&query=sushi&sources=web
		// http://api.bing.net/json.aspx?Appid=<AppID>&query=sushi&sources=web. 
		// https://api.datamarket.azure.com/Bing/SearchWeb/
			 

		 /* *************************RESPONSE ***************/
		 /*
		  * "SearchResponse":{ "Version":"2.0","Query":{

			"SearchTerms":"sushi"},"Web":{ "Total":15000000,"Offset":0,"Results":[
			
			{ "Title":"Sushi - Wikipedia, the free encyclopedia","Description":"In
			
			Japanese cuisine, sushi (寿司, 鮨, 鮓, sushi?) is vinegared rice, usually
			
			topped with other ingredients, including fish (cooked or uncooked) and
			
			vegetables.","Url":"http:\/\/en.wikipedia.org\/wiki\/Sushi","DisplayUrl
			
			":"http:\/\/en.wikipedia.org\/wiki\/Sushi","DateTime":"2008-06-
			
			09T06:42:34Z"}]}} }


		  */
		 System.out.println("testBingJson Before post ");
		 									  
	        HttpGet httpPost = new HttpGet("https://script.google.com/macros/s/AKfycby2cU7od1SQt2ZrGszY_D0Zo-HloJrBpUaJQcmfnu4J-RrreUY/exec?tab=1"/*"https://api.datamarket.azure.com/Bing/Search/Web?Query='Anil'&$top=10&$skip=20&$format=JSON"*/);
	      //  HttpClient client1 = new DefaultHttpClient();
			try {
				System.out.println("testBingJson Before getSslClient ");
				httpPost.setHeader("Content-type", "application/json");
			//	httpClient = getSslClient(client1);
				HttpResponse httpResponse = httpClient.execute(httpPost);
				System.out.println("testBingJson After httpClient.execute ");
				
		        HttpEntity httpEntity = httpResponse.getEntity();
		        System.out.println("testBingJson After httpEntity ");
		        result = EntityUtils.toString(httpEntity);
		        System.out.println("Response is "+result);
			} /*catch (KeyManagementException e) {
				// TODO Auto-generated catch block
				System.out.println("KeyManagementException is "+e.getMessage());
				e.printStackTrace();
			} catch (UnrecoverableKeyException e) {
				System.out.println("UnrecoverableKeyException is "+e.getMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				System.out.println("NoSuchAlgorithmException is "+e.getMessage());
				e.printStackTrace();
			} catch (KeyStoreException e) {
				System.out.println("KeyStoreException is "+e.getMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			} */catch (ClientProtocolException e) {
				System.out.println("ClientProtocolException is "+e.getMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("IOException is "+e.getMessage());
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Response is *****END******"+result);
		return result;
	}
	public String getDeviceID()
	{
		TelephonyManager tManager = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
		String devId = tManager.getDeviceId();
		Log.e("getDeviceID","DeviceID------------"+devId);
		return devId;
	}
	
	public static String getDeviceUid(Context c) {
        // Get the mac address and use it as deviceuid
        WifiManager wifiMan = (WifiManager) c.getSystemService(
                Context.WIFI_SERVICE);
        WifiInfo wifiInf = wifiMan.getConnectionInfo();
        String id = wifiInf.getMacAddress();

        if(id == null || id.equals("")) {
            Log.d("Test", "Failed to get the mac addr! Try with imei..");

            // Mac not found .. try imei
            TelephonyManager telephonyManager =
                    (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
            id = telephonyManager.getDeviceId();
        }

        if(id == null || id.equals("")) {
            Log.e("Test", "Failed to get mac or imei! Let use the Android_ID");

            // REMOVED this Assert, not sure why this is in non-test code?? -Ryan
            // Asserting failure, mac or imei should always be found!
            //Assert.fail("MAC address or IMEI cannot be received!");

            id=  Secure.getString(c.getContentResolver(),Secure.ANDROID_ID);
        }

        UUID uuid = null;

        try {
            uuid = UUID.nameUUIDFromBytes(id.getBytes("utf8"));
        } catch (UnsupportedEncodingException e) {
            Log.e("Test", "Failed convert to uuid");
        }

        if(uuid == null) {
            return id;
        } else {
            Log.d("Test", "uuid = " + uuid.toString());

            return uuid.toString();
        }
    }
	private String addLocationToUrl(String url){
	    if(!url.endsWith("?"))
	        url += "?";
	    String response = null;
	    ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();

	    if (lat != 0.0 && lon != 0.0){ 
	    	System.out.println("getDeviceUid:"+getDeviceUid(mContext));
	    	System.out.println("sentDeviceid:"+"9c1b11e9-ae6f-3396-9b8d-de570ceeb6ea");
	    	params.add(new BasicNameValuePair("coreContainerUuid", getDeviceUid(mContext)/* getDeviceID()*//*"9c1b11e9-ae6f-3396-9b8d-de570ceeb6ea"*/));
	    	params.add(new BasicNameValuePair("ssoToken", "AQIC5wM2LY4Sfcw8ju7eX4sczSfDnaVVhsDGbhBIU5e1tMQ.*AAJTSQACMDIAAlMxAAIwMQ..*"));
	        params.add(new BasicNameValuePair("lat", String.valueOf(lat)));
	        params.add(new BasicNameValuePair("lon", String.valueOf(lon)));
	    }
	    JSONObject body = new JSONObject();
	    try {											//l7xxb421ad3f3b6449d598c677865b4e3e3f
			body.put("coreContainerUuid",getDeviceUid(mContext)/* getDeviceID()/*"9c1b11e9-ae6f-3396-9b8d-de570ceeb6ea"*/);
			body.put("ssoToken", mSsoToken);
			
//			body.put("ssoToken", "AQIC5wM2LY4SfcxE6qowtO2J-iGebwDgGgjRZtw49IQbTfA.*AAJTSQACMDIAAlMxAAIwMQ..*");
			body.put("latitude", lat);
			body.put("longitude", lon);
			body.put("radius", 2.00);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("https://116.50.77.28:8443/servers-panelWebService/rest/location/update");

       // ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();

      //  nameValuePair.add(new BasicNameValuePair("signture", (String)parameters.get(1)));
      //  nameValuePair.add(new BasicNameValuePair("loanCode", (String) parameters.get(2)));

        try {
			//httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        										  //qwertyuiopasdfghjklzxcvbnm0123456789
			httpPost.addHeader("x-server-api-key", "qwertyuiopasdfghjklzxcvbnm0123456789");
			httpPost.addHeader("x-device-os", "ANDROID");
			httpPost.addHeader("x-device-type", "PHONE");
			httpPost.setHeader("Content-type", "application/json");
			StringEntity se = new StringEntity(body.toString());
			 
			HttpClient client1 = new DefaultHttpClient();
			httpClient = getSslClient(client1);
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			httpPost.setEntity(se);
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			
		        HttpEntity httpEntity = httpResponse.getEntity();
		        response = EntityUtils.toString(httpEntity);
		        System.out.println("Response is "+response);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
        catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      


	   // params.add(new BasicNameValuePair("user", agent.uniqueId));

	   // String paramString = URLEncodedUtils.format(params, "utf-8");

	   // url += paramString;
	    return response;
	}
	private HttpClient getSslClient(HttpClient client)
			throws KeyManagementException, UnrecoverableKeyException,
			NoSuchAlgorithmException, KeyStoreException {

		SSLContext sslContext = SSLContext.getInstance("TLS");
		SSLSocketFactory ssf = new IdentitySSLSocketFactory(sslContext);
		ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		ClientConnectionManager ccm = client.getConnectionManager();
		SchemeRegistry sr = ccm.getSchemeRegistry();
		sr.register(new Scheme("https", ssf, 8443));
		return new DefaultHttpClient(ccm, client.getParams());
	}
	
}
