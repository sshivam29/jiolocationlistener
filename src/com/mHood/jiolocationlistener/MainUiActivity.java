package com.mHood.jiolocationlistener;



import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.UUID;


import org.json.JSONException;
import org.json.JSONObject;

import com.jio.mhood.services.JioServiceManager;
import com.jio.mhood.services.JioServiceManager.JioServiceInitCallback;
import com.jio.mhood.services.api.accounts.account.AccountInfo;
import com.jio.mhood.services.api.accounts.account.AccountManager;
import com.jio.mhood.services.api.accounts.login.AuthenticationManager;
import com.jio.mhood.services.api.authorization.ProtectedApiException;
import com.jio.mhood.services.api.connection.ErrorResponse;



import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings.Secure;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainUiActivity extends Activity implements OnClickListener{
	Button start_stoop_service_btn,sendLocationInfoBtn,getLocationInfoBtn,simulateLocation;
	EditText iEditText;
	LocationManager locationManager = null;
	LocationListener loc_listener = null;
	double  lat, lon;
	private AuthenticationManager mMdkClientApi;
	private static final String SERVICE_ID = "00001";
	private Handler mHandler;
	private String mSsoToken;
	private boolean sendbtnClicked = false,simulatebtnClicked = false;
	private boolean isLoginRegistered = false;
	private Intent stopLocationServiceIntent = null;
	public static final String ANALYTICS_URI = "http://54.254.208.141";
	public static final String COUNTLY_JIO_LOCATION_APP_KEY = "df584116abfdd0a8b02de9e000489b6bb660a4e2";
	
	AlertDialog dialog = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_ui);
		
		Init();
		//InitCountlyAnalytics();//added for testing Countly
	}
	/*@Override
	protected void onResume() {
		
		Countly.sharedInstance().onStart();
		super.onResume();
	}
	@Override
	protected void onPause() {
		
		Countly.sharedInstance().onStop();
		super.onResume();
	}
	*/
	void Init()
	{
		start_stoop_service_btn =(Button)findViewById(R.id.start_stop_service_btn);
		start_stoop_service_btn.setOnClickListener(this);
		sendLocationInfoBtn =(Button)findViewById(R.id.sendlocationBtn);
		sendLocationInfoBtn.setOnClickListener(this);
		
		getLocationInfoBtn =(Button)findViewById(R.id.getLocationBtn);
		getLocationInfoBtn.setOnClickListener(this);
		
		simulateLocation = (Button) findViewById(R.id.simulatelist);
		simulateLocation.setOnClickListener(this);
		iEditText = (EditText) findViewById(R.id.locationUpdateEditeBox);
		iEditText.setEnabled(false);
		mHandler = new Handler();
		
		stopLocationServiceIntent = new Intent(this,JioLocationListenerService.class);
		stopLocationServiceIntent.setPackage("com.mHood.jiolocationlistener");
		
		Intent intent = new Intent(this,JioLocationListenerService.class);
		intent.setPackage("com.mHood.jiolocationlistener");
		startService(intent);
		JioServiceManager.init(this, new JioServiceCB());
	}
	
	/*private void InitCountlyAnalytics()
	{
		Countly.sharedInstance().init(MainUiActivity.this, ANALYTICS_URI,COUNTLY_JIO_LOCATION_APP_KEY);
	}*/
	class JioServiceCB implements JioServiceInitCallback{

		@Override
		public void onJioServicesFailed() {
			// TODO Auto-generated method stub
			//Toast.makeText(MainUiActivity.this,"onJioServicesFailed()", Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onJioServicesReady() {
			// TODO Auto-generated method stub
			 mMdkClientApi = (AuthenticationManager) JioServiceManager
		                .getJioService(JioServiceManager.AUTHENTICATION_MANAGER);
			 GetSsoTokenTask aTask = new GetSsoTokenTask();
		        aTask.execute();

			 
		}
    	
    }
	
	private Location getLastKnownLocation() {
	    List<String> providers = locationManager.getProviders(true);
	    Location bestLocation = null;
	    for (String provider : providers) {
	        Location l = locationManager.getLastKnownLocation(provider);
	        Log.d("last known location"," provider:"+provider+", location: "+l);

	        if (l == null) {
	            continue;
	        }
	        if (bestLocation == null
	                || l.getAccuracy() < bestLocation.getAccuracy()) {
	            Log.d("getLastKnownLocation","found best last known location:"+l);
	            bestLocation = l;
	        }
	    }
	    if (bestLocation == null) {
	        return null;
	    }
	    return bestLocation;
	}
	 private class GetSsoTokenTask extends AsyncTask<Void, Void, Void> {
	        @Override
	        protected Void doInBackground(Void... object) {
	            final String CONTEXT = "GetSsoTokenTask.doInBackground";
	            Log.d("GetSsoTokenTask", CONTEXT + "> ");

	            getSsoToken();

	            return null;
	        }

	      }

	    /**
	     * Negative/error callback for ssoToken request
	     */
	    private final Handler mErrorHandler = new Handler() {
	        @Override
	        public void handleMessage(Message msg) {
	            final ErrorResponse response = msg.getData().getParcelable(
	                    AuthenticationManager.JIO_ERROR);
	            String str = response.getMessage();
	            String aMessage = null;

	            if (str != null) {
	                aMessage = response.getMessage();
	            } else {
	                aMessage = "Not logged in.";
	            }

	            final String message = aMessage;

	            mHandler.post(new Runnable() {
	                @Override
	                public void run() {
	                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
	                }
	            });

	        }
	    };

	    /**
	     * Gets the SSO Token.
	     */
	    private void getSsoToken() {
	        mHandler.post(new Runnable() {
	            @Override
	            public void run() {
	                Toast.makeText(getApplicationContext(), "Invoking getSsoToken()....", Toast.LENGTH_LONG).show();
	            }
	        });
	        if(mMdkClientApi == null)
	        {
	        	 mMdkClientApi = (AuthenticationManager) JioServiceManager
			                .getJioService(JioServiceManager.AUTHENTICATION_MANAGER);
	        }
	        mMdkClientApi.getSsoTokenWithNotification(SERVICE_ID, null, mAuthenticationListener, mErrorHandler);
	    }
	    
	    private BroadcastReceiver mLoginReceiver = new BroadcastReceiver() {
	        @Override
	        public void onReceive(Context context, Intent intent) {
	            if (intent.getAction().equals(AuthenticationManager.INTENT_ACTION_JIO_LOGIN_FINISHED)) {
	                try {
	                    unregisterReceiver(this);
	                    mLoginReceiver = null;
	                } catch (Exception e) {

	                }

	                // Retry getting SSO Token ...
	                GetSsoTokenTask aTask = new GetSsoTokenTask();
	                aTask.execute();
	            }
	        }
	    };
	    private IntentFilter mLoginFilter = new IntentFilter(AuthenticationManager.INTENT_ACTION_JIO_LOGIN_FINISHED);
	    private final AuthenticationManager.AuthenticationListener mAuthenticationListener =
	            new AuthenticationManager.AuthenticationListener() {

	        /**
	         * Positive callback for ssoToken request
	         */
	        @Override
	        public void onTokenReceived(final String ssoToken) {
	            mSsoToken = ssoToken;

	            mHandler.post(new Runnable() {
	                @Override
	                public void run() {
	                    Toast.makeText(getApplicationContext(), "SSO Token: " + ssoToken, Toast.LENGTH_LONG).show();
	                    if(sendbtnClicked == true)
	                    	sendLocationUpdate();
	                    else if(simulatebtnClicked == true)
	                    {
	                    	//startSimulateActivity();
	                    	CreateAlertDialog();
	                    }
	                }
	            });

	        } 
	        /**
	         * User interaction requested to complete ssoToken request
	         *
	         * @param requestIntent
	         * @param requestCode
	         */
	        @Override
	        public void onActivityRequest(Intent requestIntent, int requestCode) {
	            final String CONTEXT = "onActivityRequest";
	            Log.i("onActivityRequest", CONTEXT + "> requestIntent: " + requestIntent + "; requestCode: " + requestCode);

	            try {
	                registerReceiver(mLoginReceiver, mLoginFilter);
	                isLoginRegistered = true;
	            } catch (Exception e) {
	                Log.e("onActivityRequest", CONTEXT, e);
	            }

	            mHandler.post(new Runnable() {
	                @Override
	                public void run() {
	                    Toast.makeText(getApplicationContext(), "User activity requested!", Toast.LENGTH_LONG).show();
	                }
	            });
	        }
	    };
	    
	    public static String getDeviceUid(Context c) {
	        // Get the mac address and use it as deviceuid
	        WifiManager wifiMan = (WifiManager) c.getSystemService(
	                Context.WIFI_SERVICE);
	        WifiInfo wifiInf = wifiMan.getConnectionInfo();
	        String id = wifiInf.getMacAddress();

	        if(id == null || id.equals("")) {
	         //   Log.d("Test", "Failed to get the mac addr! Try with imei..");

	            // Mac not found .. try imei
	            TelephonyManager telephonyManager =
	                    (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
	            id = telephonyManager.getDeviceId();
	        }

	        if(id == null || id.equals("")) {
	          //  Log.e("Test", "Failed to get mac or imei! Let use the Android_ID");

	            // REMOVED this Assert, not sure why this is in non-test code?? -Ryan
	            // Asserting failure, mac or imei should always be found!
	            //Assert.fail("MAC address or IMEI cannot be received!");

	            id=  Secure.getString(c.getContentResolver(),Secure.ANDROID_ID);
	        }

	        UUID uuid = null;

	        try {
	            uuid = UUID.nameUUIDFromBytes(id.getBytes("utf8"));
	        } catch (UnsupportedEncodingException e) {
	            Log.e("Test", "Failed convert to uuid");
	        }

	        if(uuid == null) {
	            return id;
	        } else {
	            Log.d("Test", "uuid = " + uuid.toString());

	            return uuid.toString();
	        }
	    }
	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.start_stop_service_btn)
		{
			
			
			if(stopLocationServiceIntent != null) { 
			    Toast.makeText(getBaseContext(), "Service is already running...stopping it", Toast.LENGTH_SHORT).show();
			    //start_stoop_service_btn.setBackgroundColor(0);
			    start_stoop_service_btn.setText("Start Loc Listerner");
			    stopService(stopLocationServiceIntent);
			    stopLocationServiceIntent = null;
			}else {
			    Toast.makeText(getBaseContext(), "There is no service running, starting service..", Toast.LENGTH_SHORT).show();
			   // start_stoop_service_btn.setBackgroundColor(1);
			    start_stoop_service_btn.setText("Stop Loc Listerner");
			    stopLocationServiceIntent = new Intent(this,JioLocationListenerService.class);
				stopLocationServiceIntent.setPackage("com.mHood.jiolocationlistener");
			    startService(stopLocationServiceIntent);
			}
			
		}
		else if(v.getId() == R.id.sendlocationBtn)
		{
			sendbtnClicked = true;
			System.out.println("getDeviceUid:"+getDeviceUid(getApplicationContext()));
	    	System.out.println("sentDeviceid:"+"9c1b11e9-ae6f-3396-9b8d-de570ceeb6ea");
			sendLocationUpdate();
		}
		else if(v.getId() == R.id.getLocationBtn)
		{
			AccountManager mAccountManager = (AccountManager) JioServiceManager
		                .getJioService(JioServiceManager.ACCOUNT_MANAGER);
			AccountInfo accountInfo = mAccountManager.getAccount(mSsoToken);
			try {
				Log.e("accountInfo.getMobile()","accountInfo.getMobile():"+accountInfo.getMobile());
				//accountInfo.getMobile();
				Log.e("accountInfo.getMobile()","accountInfo.getUid():"+accountInfo.getUid());
				//accountInfo.getUid();
			} catch (ProtectedApiException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			 
			getLocationUpdate();
		}
		else if(v.getId() == R.id.simulatelist)
		{
			if(mSsoToken == null)
			{
				simulatebtnClicked = true;
				getSsoToken();
				return ;
			}
			CreateAlertDialog();
			
		}
		
	}
	public void CreateAlertDialog()
	{
		// 1. Instantiate an AlertDialog.Builder with its constructor 
		AlertDialog.Builder builder = new AlertDialog.Builder(MainUiActivity.this); 
		// 2. Chain together various setter methods to set the dialog characteristics
		builder.setMessage("Background Service will be stopped.Do you want to proceed?");
		builder.setTitle("Alert");
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{            public void onClick(DialogInterface dialog, int id) 
						{
							// User clicked OK button  
							startSimulateActivity();
						}        
					});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// User cancelled the dialog
				if(dialog != null)
				{
					dialog.dismiss();
				}
				}        
			}); 
					
		// 3. Get the AlertDialog from create()
		dialog = builder.create();
		dialog.show();

	}
	public int startSimulateActivity()
	{
		if(stopLocationServiceIntent != null)
		{
			stopService(stopLocationServiceIntent);
		    stopLocationServiceIntent = null;
		    start_stoop_service_btn.setText("Start Loc Listerner");
		}
		Intent i = new Intent(MainUiActivity.this,SimulateLocationActivity.class);
		i.putExtra("sso", mSsoToken);
		Toast.makeText(this, "Main SSO:"+mSsoToken, Toast.LENGTH_SHORT).show();
		startActivity(i);
		return 0;
	}
	public int getLocationUpdate() {
		// TODO Auto-generated method stub
		Log.e("getLocationUpdate","Err------------Started");
		 	locationManager = (LocationManager)getSystemService (LOCATION_SERVICE);
		 	
		    Criteria criteria = new Criteria ();
		    String bestProvider = locationManager.getBestProvider (criteria, false);
		    Location location = locationManager.getLastKnownLocation (bestProvider);
		    loc_listener = new LocationListener() {

		        public void onProviderEnabled(String p) {
		        	Log.e("getLocationUpdate","Err------------onProviderEnabled:"+p);
		        }

		        public void onProviderDisabled(String p) {
		        	Log.e("getLocationUpdate","Err------------onProviderDisabled:"+p);
		        }

		        public void onStatusChanged(String p, int status, Bundle extras) {
		        	Log.e("getLocationUpdate","Err------------onStatusChanged:"+p);
		        }

				@Override
				public void onLocationChanged(Location l) {
					// TODO Auto-generated method stub
					lat = l.getLatitude();
					lon = l.getLongitude();
					Log.e("onLocationChanged","Err------------onLocationChanged lat:"+lat);
					Toast.makeText(getApplicationContext(), "onLocationChanged Lat:"+lat+"Lon:"+lon, Toast.LENGTH_SHORT).show();
				}      
		    };
		   // locationManager.requestLocationUpdates(bestProvider,20000 ,0, loc_listener);
		    location = locationManager.getLastKnownLocation (bestProvider);   
		    if(location == null)
		    {
		    	Log.e("getLocationUpdate","location******* NULL");
		    	location = getLastKnownLocation();
		    }
		    try
		    {
		        lat = location.getLatitude ();
		        lon = location.getLongitude ();
		        Log.d("SMSSending","SendSms called from TRY Lat:"+lat+"Lon:"+lon);	
		        Toast.makeText(getApplicationContext(), "called from TRY  Lat:"+lat+"Lon:"+lon, Toast.LENGTH_SHORT).show();
		        iEditText.setText("Latitue:"+lat+"\n Longitude:"+lon);
		    }
		    catch (NullPointerException e)
		    {
		    	Log.e("getLocationUpdate","Err------------NullPointerException:"+e.getMessage());
		        lat = -1.0;
		        lon = -1.0;
		    }
		    
		    return 0;
	}
	 
	public int sendLocationUpdate()
	{
		if(mSsoToken == null)
		{
			getSsoToken();
			return 0;
		}
		 JSONObject input = new JSONObject();
         try {
				input.put("latitude", lat);
				input.put("longitude", lon);
             input.put("ssoToken", mSsoToken);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
         
         //new asyncTask().execute("null");
         JioLocationAsyncTask iJiopoStAsyncTask = new JioLocationAsyncTask(getApplicationContext());
         iJiopoStAsyncTask.execute(input);
         sendbtnClicked = false;
		return 0;
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.e("onDestroy","Err------------onDestroy() called");
		if(locationManager != null)
		{
			if(loc_listener != null)
				locationManager.removeUpdates(loc_listener);
			locationManager = null;
		}
		final String CONTEXT = "onDestroy";
        Log.d("onDestroy", CONTEXT + "> ");
        if(mLoginReceiver != null && isLoginRegistered)
        {
	        try {
	        	
	            unregisterReceiver(mLoginReceiver);
	        } catch (Exception e) {
	            Log.e("onDestroy", CONTEXT, e);
	        }
        }

		super.onDestroy();
	}
}
